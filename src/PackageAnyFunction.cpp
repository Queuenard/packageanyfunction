#include <windows.h>
#include <psapi.h>

#include "RedoBlHooks.hpp"

bool init(){
	BlInit;

	ADDR BlScanHex(functionCheck, "80 7B 24 00 0F 85 ? ? ? ? 85 C9 0F 85 ? ? ? ? 85 C0 0F 84 ? ? ? ? 68 ? ? ? ? 50");

	//Replace with NOP then an operation that won't set the zero flag
	BlPatchBytes(4, functionCheck, "\x90\x83\xC8\xFF");


	ADDR BlScanHex(namespaceCheck, "80 7D A4 00 74");

	//Replace with NOP then an operation that will set the zero flag
	BlPatchBytes(4, namespaceCheck, "\x90\x83\xE0\x00");

	BlPrintf("PackageAnyFunction: Successfully disabled package restrictions.");

	return true;
}

int WINAPI DllMain( HINSTANCE hInstance,  unsigned long reason,  void *reserved ){
	switch(reason){
		case DLL_PROCESS_ATTACH:
			return init();
		case DLL_PROCESS_DETACH:
			return true;
		default:
			return true;
	}
}