# PackageAnyFunction

Lets you package or overwrite any function from TorqueScript. Works with r2033. Use RedBlocklandLoader.

This is a supersession of [Redo / PackageAnyFunction](https://gitlab.com/Redo0/packageanyfunction), which has not been updated beyond r2005.

### Technical details

Non-default scripts are not allowed to declare or package certain functions.

There is a check to determine whether the declared function is an allowed function name. At ```0x430A0C```, there is a check to see whether the function was declared from a default-script context. The replacement instruction causes this check to pass.

| Before | After |
| ------ | ------ |
| 0x430a0c:	cmpb   $0x0,0x24(%ebx) | 0x430a0c:	nop |
| | 0x430a0d:	or     $0xffffffff,%eax |
| 0x430a10:	jne    0x431404 | 0x430a10:	jne    0x431404 |

Just beyond this section, there are repetitive sections that check the declared function against the list of restricted functions/namespaces. ```0x431404``` is at the end of this section, hence the jump to that position at instruction ```0x430A10```.

There is a second check to determine whether the declared function is in an allowed namespace. At ```0x431404```, there is a check to see whether the function was declared from a default-script context. The replacement instruction causes this check to pass.

| Before | After |
| ------ | ------ |
| 0x431404:	cmpb   $0x0,-0x5c(%ebp) | 0x431404:	nop |
| | 0x431405:	and    $0x0,%eax |
| 0x431408:	je     0x431431| 0x431408:	je     0x431431 |

